package com.codegama.todolistapplication.model;

import android.icu.text.CaseMap;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.Relation;

import java.io.Serializable;

@Entity
public class bigTask implements Serializable {

    @PrimaryKey(autoGenerate = true)
    int taskId;
    @ColumnInfo(name = "taskList_id")
    Integer taskList_id;
    @ColumnInfo(name = "taskTitle")
    String taskTitle;
    @ColumnInfo(name = "date")
    String date;
    @ColumnInfo(name = "taskDescription")
    String taskDescription;
    @ColumnInfo(name = "subtaskTitle")
    String subtaskTitle;
    @ColumnInfo(name = "subdate")
    String subdate;
    @ColumnInfo(name = "subtaskDescription")
    String subtaskDescription;
    @ColumnInfo(name = "isComplete")
    boolean isComplete;
    @ColumnInfo(name = "firstAlarmTime")
    String firstAlarmTime;
    @ColumnInfo(name = "secondAlarmTime")
    String secondAlarmTime;
    @ColumnInfo(name = "lastAlarm")
    String lastAlarm;
    @ColumnInfo(name = "event")
    String event;

    public bigTask() {

    }

    public boolean isComplete() {
        return isComplete;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public void setComplete(boolean complete) {
        isComplete = complete;
    }

    public String getFirstAlarmTime() {
        return firstAlarmTime;
    }

    public void setFirstAlarmTime(String firstAlarmTime) {
        this.firstAlarmTime = firstAlarmTime;
    }

    public String getSecondAlarmTime() {
        return secondAlarmTime;
    }

    public void setSecondAlarmTime(String secondAlarmTime) {
        this.secondAlarmTime = secondAlarmTime;
    }

    public String getLastAlarm() {
        return lastAlarm;
    }

    public void setLastAlarm(String lastAlarm) {
        this.lastAlarm = lastAlarm;
    }

    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    public int gettaskList_id() {
        return taskList_id;
    }

    public void SettaskList_id(int taskId) {
        this.taskList_id = taskId;
    }

    public String getTaskTitle() {
        return taskTitle;
    }

    public void setTaskTitle(String taskTitle) {
        this.taskTitle = taskTitle;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTaskDescription() {
        return taskDescription;
    }

    public void setTaskDescription(String taskDescription) {
        this.taskDescription = taskDescription;
    }

    public String getsubTaskTitle() {
        return subtaskTitle;
    }

    public void setsubTaskTitle(String subtaskTitle) {
        this.subtaskTitle = taskTitle;
    }

    public String getsubDate() {
        return subdate;
    }

    public void setsubDate(String subdate) {
        this.subdate = subdate;
    }

    public String getsubTaskDescription() {
        return subtaskDescription;
    }

    public void setsubTaskDescription(String subtaskDescription) {
        this.subtaskDescription = subtaskDescription;
    }
}
