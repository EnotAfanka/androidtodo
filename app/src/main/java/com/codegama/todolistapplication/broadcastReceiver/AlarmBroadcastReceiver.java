package com.codegama.todolistapplication.broadcastReceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.legacy.content.WakefulBroadcastReceiver;

import com.codegama.todolistapplication.R;
import com.codegama.todolistapplication.activity.AlarmActivity;

public class AlarmBroadcastReceiver extends BroadcastReceiver {

    String title, desc, date, time;
    @Override
    public void onReceive(Context context, Intent intent) {

        title = intent.getStringExtra("НАЗВАНИЕ");
        desc = intent.getStringExtra("ОПИСАНИЕ");
        date = intent.getStringExtra("ДАТА");
        time = intent.getStringExtra("ВРЕМЯ");

        Intent i = new Intent(context, AlarmActivity.class);
        i.putExtra("НАЗВАНИЕ", title);
        i.putExtra("ОПИСАНИЕ", desc);
        i.putExtra("ДАТА", date);
        i.putExtra("ВРЕМЯ", time);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i);
    }
}
